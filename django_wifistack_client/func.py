import requests
import os


def wifistack_url():
    try:
        from django.conf import settings
    except ImportError:
        return os.getenv(
            "WIFISTACK_API_URL", default="https://wifistack-api.example.com/"
        )

    return settings.WIFISTACK_API_URL


def wifistack_default_credentials():
    try:
        from django.conf import settings
    except ImportError:
        username = os.getenv("WIFISTACK_API_USER", default="user")
        password = os.getenv("WIFISTACK_API_PASSWD", default="secret")
        return username, password

    username = settings.WIFISTACK_API_USER
    password = settings.WIFISTACK_API_PASSWD

    return username, password


def wifistack_head(endpoint: str, api_user: str, api_password: str, params=None):
    if params is None:
        params = dict()

    response = requests.head(
        f"{wifistack_url()}/{endpoint}",
        headers={
            "Authorization": "ApiKey {}:{}".format(
                api_user,
                api_password,
            ),
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        params=params,
    )

    return response.headers


def wifistack_get_data(endpoint: str, api_user: str, api_password: str, params=None):
    if params is None:
        params = dict()

    response = requests.get(
        f"{wifistack_url()}/{endpoint}",
        headers={
            "Authorization": "ApiKey {}:{}".format(
                api_user,
                api_password,
            ),
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        params=params,
    )

    return response.json()


def wifistack_post_data(
    endpoint: str, api_user: str, api_password: str, params=None, data=None
):
    if params is None:
        params = dict()

    if data is None:
        data = dict()

    response = requests.post(
        f"{wifistack_url()}/{endpoint}",
        headers={
            "Authorization": "ApiKey {}:{}".format(
                api_user,
                api_password,
            ),
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        params=params,
        json=data,
    )

    return response.json()


def wifistack_patch_data(
    endpoint: str, api_user: str, api_password: str, params=None, data=None
):
    if params is None:
        params = dict()

    if data is None:
        data = dict()

    response = requests.patch(
        f"{wifistack_url()}/{endpoint}",
        headers={
            "Authorization": "ApiKey {}:{}".format(
                api_user,
                api_password,
            ),
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        params=params,
        json=data,
    )

    return response.json()


def wifistack_put_data(
    endpoint: str, api_user: str, api_password: str, params=None, data=None
):
    if params is None:
        params = dict()

    if data is None:
        data = dict()

    response = requests.patch(
        f"{wifistack_url()}/{endpoint}",
        headers={
            "Authorization": "ApiKey {}:{}".format(
                api_user,
                api_password,
            ),
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        params=params,
        json=data,
    )

    return response.json()


def wifistack_delete_data(endpoint: str, api_user: str, api_password: str, params=None):
    if params is None:
        params = dict()

    response = requests.delete(
        f"{wifistack_url()}/{endpoint}",
        headers={
            "Authorization": "ApiKey {}:{}".format(
                api_user,
                api_password,
            ),
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        params=params,
    )

    if response.status_code == 200:
        return True

    return False


def wifistack_get_api_credentials_for_customer(mdat_id: int):
    username, password = wifistack_default_credentials()

    if username != mdat_id:
        response = wifistack_get_data(
            f"v2/apiauthcustomerapikey/{mdat_id}",
            username,
            password,
        )

        username = str(response["user"]["id"])
        password = response["key"]

    return username, password


def wifistack_get_wireless_nodes(mdat_id: int):
    username, password = wifistack_get_api_credentials_for_customer(mdat_id)

    response = wifistack_get_data(
        "v2/wirelessnodeadmin",
        username,
        password,
    )

    if "objects" not in response:
        return list()

    return response["objects"]


def wifistack_get_wireless_node(mdat_id: int, node_id: int):
    username, password = wifistack_get_api_credentials_for_customer(mdat_id)

    response = wifistack_get_data(
        f"v2/wirelessnodeadmin/{node_id}",
        username,
        password,
    )

    return response


def wifistack_update_wireless_node(mdat_id: int, node_id: int, settings: dict):
    username, password = wifistack_get_api_credentials_for_customer(mdat_id)

    response = wifistack_patch_data(
        f"v2/wirelessnodeadmin/{node_id}",
        username,
        password,
        data=settings,
    )

    return response


def wifistack_get_domains(mdat_id: int):
    username, password = wifistack_get_api_credentials_for_customer(mdat_id)

    response = wifistack_get_data(
        "v2/domain",
        username,
        password,
    )

    if "objects" not in response:
        return list()

    return response["objects"]
